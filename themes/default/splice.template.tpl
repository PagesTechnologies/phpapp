<!-- BEGIN: tpl -->


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{TITLE}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="ru" />
<meta http-equiv="Cache-Control" content="private" />

<meta name="description" content="{META_DESCRIPTION}" />
<meta http-equiv="description" content="{META_DESCRIPTION}" />
<meta name="Keywords" content="{META_KEYWORDS}" />
<meta http-equiv="Keywords" content="{META_KEYWORDS}" />
<meta name="Resource-type" content="document" />
<meta name="document-state" content="dynamic" />
<meta name="Robots" content="index,follow" />
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Bootstrap 
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-wip/css/bootstrap.min.css">-->
	<!--<link rel="stylesheet" href="{URL}/plugins/bootstrap/bootstrap.css">-->
	<link rel="stylesheet" href="{URL}/plugins/bootstrap3/css/bootstrap.css">
	<link rel="stylesheet" href="{URL}/plugins/bootstrap3/css/sticky-footer-navbar.css">
	
	<link rel="stylesheet" href="{URL}/plugins/bootstrap/font-awesome.css">
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{URL}/plugins/jquery/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed 
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-wip/js/bootstrap.min.js"></script>-->
	<!--<script src="{URL}/plugins/bootstrap/bootstrap.js"></script>-->
	<script src="{URL}/plugins/bootstrap3/js/bootstrap.js"></script>
	
    <!-- Enable responsive features in IE8 with Respond.js (https://github.com/scottjehl/Respond) 
    <script src="{URL}/plugins/bootstrap/js/respond.js"></script>-->

<!--plugin fancybox-->
	<link rel="stylesheet" href="{URL}/plugins/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="{URL}/plugins/fancybox/source/jquery.fancybox.js"></script>
	<script type="text/javascript" src="{URL}/plugins/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!--END plugin fancybox-->



<!--plugin respond -->
	<script type="text/javascript" src="{URL}/plugins/respond/respond.js"></script>
<!--END plugin respond -->





<!--
<style>

.myheader {
        background: url({URL}/themes/{THEME}/img/body.png) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
}

</style>
-->


<style>
html{
background: url({URL}/themes/{THEME}/img/fon.jpg) repeat;
height:100% !important;
}
body {
	padding: 0;
	margin: 0;
        background: url({URL}/themes/{THEME}/img/body.jpg) no-repeat  center top ;
        width: 100%;
		height:100% !important;
	display: table;
	
	
	
	
}

.grad-content{
	background: -moz-linear-gradient(#e4dce9, white); /* FF 3.6+ */
	background: -ms-linear-gradient(#e4dce9, white); /* IE10 */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #e4dce9), color-stop(100%, #ffffff)); /* Safari 4+, Chrome 2+ */
	background: -webkit-linear-gradient(#e4dce9, white); /* Safari 5.1+, Chrome 10+ */
	background: -o-linear-gradient(#e4dce9, white); /* Opera 11.10 */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e4dce9', endColorstr='#ffffff'); /* IE6 &amp; IE7 */
	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr='#e4dce9', endColorstr='#ffffff')"; /* IE8+ */
	background: linear-gradient(#e4dce9, white); /* the standard */

}

.grad-submenu{
	background: -moz-linear-gradient(#c3c0e0, white); /* FF 3.6+ */
	background: -ms-linear-gradient(#c3c0e0, white); /* IE10 */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #c3c0e0), color-stop(100%, #ffffff)); /* Safari 4+, Chrome 2+ */
	background: -webkit-linear-gradient(#c3c0e0, white); /* Safari 5.1+, Chrome 10+ */
	background: -o-linear-gradient(#c3c0e0, white); /* Opera 11.10 */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#c3c0e0', endColorstr='#ffffff'); /* IE6 &amp; IE7 */
	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr='#c3c0e0', endColorstr='#ffffff')"; /* IE8+ */
	background: linear-gradient(#c3c0e0, white); /* the standard */

}

</style>

<!--init style for tinymce image mysetting fullwidth=100% -->
<style>
.fullwidth{width:100%;}
</style>
<!--END plugin code -->

<script type="text/javascript" src="{URL}/plugins/jquery/jquery.json-1.3.js"></script>



<script type="text/javascript" src="{URL}/js/kernel.class.js"></script>


</head>







 <body >





	
		
<div class="container  " style="background: #ffffff; margin: 220px auto 0; width: 970px !important;  ">	
	
	<!--старое расположение меню
	<div class="row " >
		<div class="col-md-4 col-sm-4" >
				 <h4><span class="glyphicon glyphicon-book"> </span> {SITE_NAME}</h4>
		</div>
		<div class="col-md-8 col-sm-8">
			{mod_mainmenu:numBestLinks=5}
		</div>
	</div>
	-->
	
	
	<!--старое расположение картинки логотипа
	<div class="row ">
		<div class="col-md-12 col-sm-12">
			<img src="{URL}/themes/{THEME}/img/body.jpg" alt="..." class="img-rounded img-responsive center-block">
		</div>
	</div>
	-->
	
	<div class="row " style="background:#c3c0e0" >
		<div class="col-md-12 col-sm-12">
			{mod_mainmenu:numBestLinks=9}
		</div>
		<!--<div class="col-md-3 col-sm-3">
			 Button trigger modal 
			<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#modalWindow">
				Актуальная информация о Вузе
			</button>
		</div>-->
	</div>
	


	
	

	

	<div class="row" style="background:#e4dce9">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<p><!--пустая строка разделитель--></p>
		</div>
	</div>

	<div class="row grad-content">
		<div class="col-md-12 col-sm-12 col-xs-12 "  id="content">
			{mod_content:var1=test_10}
			{CTRL_DATA}
		</div>
  
		
	</div>

	
	
	<!-- <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="alert alert-dismissable ">
  				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				Страница сгенерирована за <strong>{pageGenerate}</strong> секунд
			</div>
		</div>
	</div> -->
	
	<div class="row">
		<div class="col-md-10 col-sm-10 col-xs-10">
			<p class="text-muted text-center">&copy; Смоленский филиал ГБОУ ВПО 'РЭУ им. Г.В. Плеханова' 2014</p>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2" >
			<div class="pull-right"	id="scroller"  > 
				<button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-up"></span> Наверх</button> 
			</div>
		</div>
	</div>
	
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


</div>
	<!-- подвал не работает с height:100% !important; в html и body
	<div id="footer">
		<div class="container"  >
			<div class="row">
				<div class="col-md-10 col-sm-10 col-xs-10">
					<p class="text-muted text-center">&copy; Смоленский филиал ГБОУ ВПО 'РЭУ им. Г.В. Плеханова' 2014</p>
				</div> 	
				<div class="col-md-2 col-sm-2 col-xs-2" >
					<div class="pull-right"	id="scroller"  > 
						<button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-up"></span> Наверх</button> 
					</div>
				</div> 
			</div>
		</div>	
    </div>
	-->
	
	
	
	
  </body>

</html>



<!-- END: tpl -->
