(function($){
  jQuery.fn.upload = function(options){
    
	
	options = $.extend({
      action:'upload.handler.php', 
      enctype: 'multipart/form-data', 
	  method: 'POST'
    }, options);
	

	return new $.initUpload(this, options);
  },
  
  $.initUpload=function(elem, options){
	
	//alert('RUN');
	elem.submit(function(){//нажатие кнопки на форме
		//alert('SUBMIT');
		
		$('#hiddenPathDir').val($('#buff_parentFolder').text());//сохраняем путь до папки в скрытом поле формы
		
		var id = new Date().getTime().toString().substr(8);
		var iframe = $('<iframe id="iframe'+id+'" name="iframe'+id+'"></iframe>').css({display: 'none'});
		//var iframe = $('<iframe id="iframe'+id+'" name="iframe'+id+'"></iframe>');
		$(this).after(iframe);
		
		$(this).attr("action",options.action);
		$(this).attr("method",options.method);
		$(this).attr("enctype",options.enctype);
		$(this).attr("target",'iframe'+id);
		
		
		
		
		
		 $('#iframe'+id).load(function(){
			var response= $(this).contents().text();
			parse(response);// from API
			ViewFolder('empty','refresh');//from dialog.handler.js
			clearUploadForm();//from dialog.handler.js
			//$('#iframe'+id).remove();
		});

		
		
	});
  
  
  
  }
  
  
  
  
})(jQuery);
