<?php 
	define('dbHOSTNAME',			"localhost");
	define('dbUSERNAME',			"");
	define('dbUSERPASSWORD',		"");
	define('dbDB',					"");

	define('URL',					"http://pages.pagecms.ru");

	define('pageName',				"Pages");
	define('META_KEYWORDS',			"Pages Pages Technologies Система управления контентом Информация");
	define('META_DESCRIPTION',		"Pages Pages Technologies Система управления контентом Информация");

	define('DOCUMENT_ROOT',			$_SERVER["DOCUMENT_ROOT"]);
	define('TEMP',					$_SERVER["DOCUMENT_ROOT"]."/temp/");
	define('MODULES',				$_SERVER["DOCUMENT_ROOT"]."/modules/");

	define('THEME',					'default');
	define('ADM_THEME',				'admin_default'); 
	define('MAX_IMG_COUNT',			8);
	define('MAX_SQLQUERY_ITEMS_LIMIT',	10);
	define('LINES_IN_PAGE',			10);
	define('LENGHT_LINK_PAGES',			2);
	define('MAX_COUNT_IN',			5);
?>
