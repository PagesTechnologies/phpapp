﻿<!-- BEGIN: tpl -->


<head>
<title>404</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="ru" />
<meta http-equiv="Cache-Control" content="private" />
<meta name="description" content="{META_DESCRIPTION}" />
<meta http-equiv="description" content="{META_DESCRIPTION}" />
<meta name="Keywords" content="{META_KEYWORDS}" />
<meta http-equiv="Keywords" content="{META_KEYWORDS}" />
<meta name="Resource-type" content="document" />
<meta name="document-state" content="dynamic" />
<meta name="Robots" content="index,follow" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">



</head>

<body>
   <div class="container ">
             <div class="row">
		<div class="col-md-6 col-sm-6">	
			<h1><p class="text-danger"> <strong>ОШИБКА 404</strong> </p></h1>
			<h3>
				<p class="text-info">Страницы которую вы пытаетесь посмотреть, тут нет.</p>
				<p class="text-info">Зато есть <a href="{URL}"><strong>главная страница</strong></a></p> 
			</h3>
		</div>	
	     </div>


   </div>

</body>
</html>


<!-- END: tpl -->
