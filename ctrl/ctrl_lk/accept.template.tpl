<!-- BEGIN: tpl -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{TITLE}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="ru" />
<meta http-equiv="Cache-Control" content="private" />
<meta name="description" content="{_META_DESCRIPTION}" />
<meta http-equiv="description" content="{META_DESCRIPTION}" />
<meta name="Keywords" content="{META_KEYWORDS}" />
<meta http-equiv="Keywords" content="{META_KEYWORDS}" />
<meta name="Resource-type" content="document" />
<meta name="document-state" content="dynamic" />
<meta name="Robots" content="index,follow" />

<link rel="stylesheet" href="{URL}/plugins/bootstrap3/css/bootstrap.css">
<link rel="stylesheet" href="{URL}/plugins/bootstrap3/css/sticky-footer-navbar.css">

<script src="{URL}/plugins/jquery/jquery-3.3.1.min.js"></script>
<script src="{URL}/plugins/bootstrap3/js/bootstrap.js"></script>
<script type="text/javascript" src="{URL}/plugins/jquery/jquery.json-1.3.js"></script>
<script type="text/javascript" src="{URL}/js/api.class.js"></script>
<script type="text/javascript" src="{URL}/ctrl/ctrl_lk/accept.handler.js"></script>

</head>


<body>
<div class="container ">
	{rulesRow}
	
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="btn btn-primary pull-right" id="buttonRulesYes" onClick="queryAcceptYes('{orgToken}'); return false;">Принять</div>
			<div class="btn  pull-right" id="buttonRulesNo" onClick="queryAcceptNo(); return false;">Отклонить</div>
		</div>
	</div>
</div>
	
</div>

<body>

<html>
<!-- END: tpl -->