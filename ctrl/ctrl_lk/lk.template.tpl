<!-- BEGIN: tpl -->


<!-- BEGIN: trItemRow -->
	
	<tr id="trItem_{itemId}">
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" value="{itemId}" class="itemCheckbox">
				</label>
			</div>
			<!--{itemId}-->
		</td>
		<td>
			<div class="row" id="divTextItemName_{itemId}">
				<div class="col-md-12 col-sm-12">
					<span id="textItemName_{itemId}">{itemName}</span>
				</div>
			</div>
			<div class="row" style="display: none;" id="divInputItemName_{itemId}">
				<div class="col-md-12 col-sm-12">
					<form role="form">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="inputItemName_{itemId}" name="inputItemName_{itemId}" placeholder="">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-default" onClick="queryItemNameSave('{itemId}'); return false;"><span class="glyphicon glyphicon-floppy-disk"></span></button>
							</span>
						</div>
					</form>
				</div>
			</div>
			
			
			
		</td>
		<td>
			<div class="btn-group">
				<button type="button " class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
					Редактировать
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
				<li><a href="#" onClick="editItemNameShow('{itemId}'); return false;"><span class="glyphicon glyphicon-pencil"></span>Редактировать заголовок</a></li>
				<li><a href="#" onClick="editItemContentShow('{itemId}'); return false;"><span class="glyphicon glyphicon-pencil"></span>Редактировать текст</a></li>
				<li><a href="#" onClick="editItemDateShow('{itemId}'); return false;"><span class="glyphicon glyphicon-pencil"></span>Редактировать время публикации</a></li>
				<li><a href="#" onClick="itemPubChenge('{itemId}','1'); return false;"><span class="glyphicon glyphicon-star"></span>Опубликовать</a></li>
				<li><a href="#" onClick="itemPubChenge('{itemId}','0'); return false;"><span class="glyphicon glyphicon-star-empty"></span>Снять с бупликации</a></li>
				<!--<li><a href="#" onClick="itemDelete('{itemId}'); return false;"><span class="glyphicon glyphicon-remove"></span>Удалить</a></li> -->
				<li><a href="#" onClick="itemDeleteModal('{itemId}'); return false;"><span class="glyphicon glyphicon-remove"></span>Удалить</a></li>
				</ul>
			</div>
		
		</td>
		<td>
			<div class="row" id="divTextDate-{itemId}">
				<div class="col-md-12 col-sm-12">
					<div id="itemDateFrom-{itemId}">{itemDateTextFrom}</div>
					<div id="itemDateTo-{itemId}">{itemDateTextTo}</div>
				</div>
				
			</div>
			<div class="row" id="divInputDate-{itemId}" style="display: none;">
				<div class="col-md-12 col-sm-12 col-lg-12">
					<form class="form-inline" role="form">
						<div class="form-group">
							<input type="date" class="form-control calendarFrom " name="calendarFrom-{itemId}" id="calendarFrom-{itemId}"   >
						</div>
						<div class="form-group">
							<input type="date" class="form-control calendarTo" name="calendarTo-{itemId}" id="calendarTo-{itemId}"   >
						</div>
						<button type="submit" class="btn btn-default" onClick="queryItemTimePubSave('{itemId}'); return false;"><span class="glyphicon glyphicon-floppy-disk"></span></button>
					</form>
				</div>
			</div>
		</td>
		<td>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-lg-12">
					<span class="glyphicon {itemPubFlag}" id="pub_{itemId}"></span>
				</div>
			</div>
		</td>
		<td></td>
	</tr>
<!-- END: trItemRow -->


<!-- BEGIN: itemPubFlagYes -->
glyphicon-star
<!-- END: itemPubFlagYes -->

<!-- BEGIN: itemPubFlagNo -->
glyphicon-star-empty
<!-- END: itemPubFlagNo -->

<!-- BEGIN: itemImg -->
<td  height="100px" width="100px" >
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 "  >
			<img src="{imgURL}" alt="Изображение">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 "  >
			<button type="button" class="btn btn-default btn-sm" onClick="imgDelete('{itemId}','{imgId}'); return false;">
				<span class="glyphicon glyphicon-remove"></span>Удалить
			</button>
		</div>
	</div>
</td>
<!-- END: itemImg -->

<!-- BEGIN: itemImgVacant -->
<td height="100px" width="100px">
<img src="{URL}/themes/{THEME}/img/item-img-vacant.png" alt="Изображение">
</td>
<!-- END: itemImgVacant -->

<!-- BEGIN: itemImgEmpty -->
<td></td>
<!-- END: itemImgEmpty -->

<!-- BEGIN: rowPreviewImgs -->
<tr>
	{tdPreviewImgs}
</tr>
<!-- END: rowPreviewImgs -->

<!-- BEGIN: errorMaxImgCount -->
<div class="alert alert-warning">Лимит прикрепленных изображений - <b>{MAX_IMG_COUNT}</b></div>
<!-- END: errorMaxImgCount -->

<!-- BEGIN: errorNotImg -->
<div class="alert alert-danger">Вы можете прикреплять только изображения</div>
<!-- END: errorNotImg -->


<!-- BEGIN: tablePreviewImgs -->
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-hover" id="tablePreviewImgs">
								<tbody>
									{trPreviewImgs}
								</tbody>
							</table>
						</div>
					</div>
				</div>
<!-- END: tablePreviewImgs -->


<!-- BEGIN: firstPAGE -->
	<li><a href=# onClick="queryGetPages(1); return false;" class="btn btn-default"><<</a></li>
<!-- END: firstPAGE -->

<!-- BEGIN: previousPAGE -->
	<li><a href=# onClick="queryGetPages({PAGE}-1); return false;" class="btn btn-default"><</a></li>
<!-- END: previousPAGE -->

<!-- BEGIN: ListPAGES -->
	<!-- BEGIN: TEXT -->
		<li class="active"><a href="#" class="btn btn-default">{PAGE}</a></strong></li>
	<!-- END: TEXT -->
	<!-- BEGIN: LINK -->
		<li><a href=# onClick="queryGetPages('{PAGE}'); return false;" class="btn btn-default " >{PAGE}</a> </li>
	<!-- END: LINK -->
<!-- END: ListPAGES -->

<!-- BEGIN: nextPAGE -->
	<li><a href=# onClick="queryGetPages({PAGE}+1); return false;" class="btn btn-default">></a></li>
<!-- END: nextPAGE -->

<!-- BEGIN: lastPAGE -->
	<li><a href=# onClick="queryGetPages('{PAGE}'); return false;" class="btn btn-default">>></a></li>
<!-- END: lastPAGE -->


<!-- BEGIN: noPAGINATION -->
	<div class="col-md-12 col-sm-12">
	<ul class="pagination pagination-sm " >
	<li><a href="#" class="btn btn-default disabled" >Страниц:({PAGES}) </a></li>
	</ul>	
	</div>
<!-- END: noPAGINATION -->



<!-- BEGIN: PAGINATION -->

	<div class="col-md-12 col-sm-12">
	<ul class="pagination pagination-sm " >
	<li><a href="#" class="btn btn-default disabled" >Страниц:({PAGES}) </a></li>
		{firstPAGE}
		{previousPAGE}
		{ListPAGES}
		{nextPAGE} 
		{lastPAGE}
	</ul>	
	</div>

<!-- END: PAGINATION -->

<!-- BEGIN: tableItems -->
	
	<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="table-responsive">
        <table class="table table-hover" id="tableItems">
            <thead>
                <tr>
                  <th>#</th>
                  <th>Наименование</th>
				  <th>Редактирование</th>
                  <th>
					Время публикации
				  </th>
                  <th>Опубликовано</th>
                  
                </tr>
            </thead>
            <tbody>
                {trDATA}
            </tbody>
        </table>
    </div>
	</div>

<!-- END: tableItems -->


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{TITLE}</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="ru" />
<meta http-equiv="Cache-Control" content="private" />
<meta name="description" content="{_META_DESCRIPTION}" />
<meta http-equiv="description" content="{META_DESCRIPTION}" />
<meta name="Keywords" content="{META_KEYWORDS}" />
<meta http-equiv="Keywords" content="{META_KEYWORDS}" />
<meta name="Resource-type" content="document" />
<meta name="document-state" content="dynamic" />
<meta name="Robots" content="index,follow" />

<!-- Bootstrap 
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-wip/css/bootstrap.min.css">-->
	<!--<link rel="stylesheet" href="{URL}/plugins/bootstrap/bootstrap.css">-->
	<link rel="stylesheet" href="{URL}/plugins/bootstrap3/css/bootstrap.css">
	<link rel="stylesheet" href="{URL}/plugins/bootstrap3/css/sticky-footer-navbar.css">
	
	<!--<link rel="stylesheet" href="{URL}/plugins/bootstrap/font-awesome.css">-->
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{URL}/plugins/jquery/jquery-3.3.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed 
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-wip/js/bootstrap.min.js"></script>-->
	<!--<script src="{URL}/plugins/bootstrap/bootstrap.js"></script>-->
	<script src="{URL}/plugins/bootstrap3/js/bootstrap.js"></script>
	
    <!-- Enable responsive features in IE8 with Respond.js (https://github.com/scottjehl/Respond) 
    <script src="{URL}/plugins/bootstrap/js/respond.js"></script>-->



<!--DROPLOADER-->
<link rel="stylesheet" href="{URL}/plugins/droploader/css/droploader.css">
<script src="{URL}/plugins/droploader/js/dmuploader.js"></script>






<script type="text/javascript" src="{URL}/plugins/jquery/jquery.json-1.3.js"></script>

<script type="text/javascript" src="{URL}/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{URL}/plugins/ckeditor/config.js"></script>

<script type="text/javascript" src="{URL}/js/api.class.js"></script>
<script type="text/javascript" src="{URL}/ctrl/ctrl_lk/lk.handler.js"></script>



</head>

<body>

<div class="container" style="width: 1170px !important;">

<nav class="navbar navbar-default navbar-collapse-top navbar-inverse " role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
			<span class="sr-only">Навигация</span>
			<span class="icon-bar"></span>
		</button>
	
	
		<a class="navbar-brand" style="padding:10px 15px" href="#"><img src="{URL}/themes/{THEME}/pyimgs/512_512.png" width="30px" height="30px">&nbsp;PAGES</a>
		
		
	</div>
	
	<div class="collapse navbar-collapse navbar-right"  id="bs-navbar-collapse-1">
		<ul class="nav navbar-nav navbar-right">
			<li class="active"><a href="#">Личный кабинет пользователя {TITLE}</a></li>
		</ul>
	</div>	

</nav>
	
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div id="anchorTop"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 "  >
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<span class="glyphicon glyphicon-unchecked">
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
				<li><a href="#" onClick="buttonChekedItem('1'); return false;"><span class="glyphicon glyphicon-ok-sign"></span>Выделить все</a></li>
				<li><a href="#" onClick="buttonChekedItem('0'); return false;"><span class="glyphicon glyphicon-ok-circle"></span>Снять выделение</a></li>
				</ul>
			</div>
			
			
			<button type="submit" class="btn btn-default" id="buttonCreateItem"><span class="glyphicon glyphicon-plus"></span>Создать запись</button>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					Редактировать
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
				<li><a href="#" onClick="itemPubChengeSelected('1'); return false;"><span class="glyphicon glyphicon-star"></span>Опубликовать</a></li>
				<li><a href="#" onClick="itemPubChengeSelected('0'); return false;"><span class="glyphicon glyphicon-star-empty"></span>Снять с бупликации</a></li>
				<li><a href="#" onClick="itemDeleteSelectedModal(); return false;"><span class="glyphicon glyphicon-remove"></span>Удалить</a></li>
				</ul>
			</div>
			<button type="submit" class="btn btn-default pull-right" id="buttonLogOut"><span class="glyphicon glyphicon-log-out"></span>Выход</button>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div id="anchorTableItems"></div>
		</div>
	</div>
	
	<div class="row"><span id="loader-itemsTable" style="display: none;"><img src="{URL}/themes/{THEME}/img/spiner-loader.gif"> Загрузка...</span></div>
	<div class="row" id="itemsTable"></div>
	<div class="row" id="itemsPages"></div>




<div class="row">
	<div class="col-md-12 col-sm-12">
		<div id="anchorForms"></div>
	</div>
</div>



	


	<div class="panel panel-default" id="panelItemCreateOrEdit" style="display: none;">
		<div class="panel-heading">
			<div class="row" style="display: none;" id="formItemLegendCreate">
				<div class="col-md-12 col-sm-12">
					<h3>Форма создания страницы</h3>
				</div>
			</div>
	
			<div class="row" style="display: none;" id="formItemLegendEdit">
				<div class="col-md-12 col-sm-12">
					<h3>Форма редактирования страницы</h3>
				</div>
				
			</div>
		</div>
		<div class="panel-body">

			<div class="row" style="display: none;" id="formItemPartName">
				<div class="col-md-12 col-sm-12">
					<div class="form-group">
						<!--<label for="item_name">Название</label>-->
						<div class="input-group">
						<span class="input-group-addon">Заголовок</span>
							<input type="text" class="form-control" placeholder="Введите заголовок" id="item_name" name="item_name"   >
							
						</div>
						<div  id="accept_item_name"  ></div>
						<div  id="notice_item_name"  ></div>
						<div  id="error_item_name"  ></div>
					</div>
				
					<!--<div class="form-group">
						<label for="item_translit">Translit</label>
						<input type="text" class="form-control" id="item_translate" name="item_translate"   >
						<div  id="accept_item_translit" class="div_textAccept" ></div>
						<div  id="notice_item_translit" class="div_textNotice" ></div>
						<div  id="error_item_translit" class="div_textError" ></div>
					</div>-->
				</div>
			</div>	
	
			<div class="row" style="display: none;" id="formItemPartContent">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div id="error_preview_errors"></div>
						</div>
					
					</div>
			
				<div class="col-md-6 col-sm-6">
					<textarea name="item_content" cols="50" rows="15" id="item_content">Контент страницы</textarea>
				</div>
		
				<div class="col-md-6 col-sm-6 ">
				<!-- D&D Zone-->
				<div id="drag-and-drop-zone" class="uploader ">
					<div>Перетащите файлы сюда</div>
					<div class="or">или</div>
					<div class="browser">
					<label>
						<span>Нажмите для выбора файлов</span>
						<input type="file" name="files[]" multiple="multiple" title='Нажмите для выбора файлов'>
					</label>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<span id="loader-preview" style="display: none;"><img src="{URL}/themes/{THEME}/img/spiner-loader.gif"> Загрузка...</span>
						</div>
					</div>
					<div id='preview_files'>
							<!--<span class="demo-note">Пока не загружено ни одного файла</span> -->
					</div>
				</div>
				<!-- /D&D Zone -->
				</div>
			</div>
	
	
		</div>
	</div>	
	
	
	
	







	<div class="row">
		<div class="col-md-12 col-sm-12">
			<p class="text-primary">Раздел служебной информации</p>
			<div id="buff_reWriteItemId">empty</div>
			<div id="buff_PageId">empty</div>
			<div id="buff_calendarFrom">empty</div>
		</div>
	</div>
<hr>
	
	

<!-- Modal -->
<div class="modal fade" id="modalItemDelete" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalLabel">Внимание</h4>
      </div>
      <div class="modal-body">
		<span id="modalItemDeleteId"></span>
        Вы действительно хотите удалить запись<br>
		<b><span id="modalItemDeleteName"></span></b>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnModalItemDelete">Удалить</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Не удалять</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="modalItemDeleteSelected" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalLabel">Внимание</h4>
      </div>
      <div class="modal-body">
        Вы действительно хотите удалить выделенные записи<br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnModalItemDeleteSelected">Удалить</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Не удалять</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="modalItemEditClose" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title" id="modalLabel">Внимание</h4>
      </div>
      <div class="modal-body">
		<span id="modalItemDeleteId"></span>
        Сохранить запись перед закрытием?<br>
		<b><span id="modalItemDeleteName"></span></b>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary"  id="btnModalItemEditSave">Сохранить</button>
        <button type="button" class="btn btn-default" id="btnModalItemEditNotSave">Не сохранять</button>
		<button type="button" class="btn btn-default" id="btnModalItemEditCansel">Отмена</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



</div>
</body>



<!-- END: tpl -->