$(document).ready(function(){

//insertTrItemRow();
//$("#testRem").remove();
//queryGetView(0);
queryGetPages(1);






$("#buttonCreateItem").click(function () {
	clearForms();
	hideForms();
	scrolltoForms();
	$("#panelItemCreateOrEdit").show();
	$("#formItemLegendCreate").show();
	$("#formItemPartName").show();
	$("#formItemPartContent").show();


	
});




$("#translateItemName").click(function(){
		translate('item_name','item_translate');
});

$("#btnModalItemDelete").click(function(){
		itemDelete($('#modalItemDeleteId').html());
		$('#modalItemDelete').modal('hide');
		$('#modalItemDelete').on('hidden.bs.modal', function (e) {
			clearModal();
		})
});

$("#btnModalItemEditSave").click(function(){
		queryItemSave();
		$('#modalItemEditClose').modal('hide');
		if($('#buff_reWriteItemId').text()=="empty"){
			scrolltoAnchor("anchorTop");
		}else{
			scrolltoAnchor("trItem_"+$('#buff_reWriteItemId').text());
		}
		
		hideForms()
		clearForms();
});

$("#btnModalItemEditNotSave").click(function(){
		$('#modalItemEditClose').modal('hide');
		if($('#buff_reWriteItemId').text()=="empty"){
			scrolltoAnchor("anchorTop");
		}else{
			scrolltoAnchor("trItem_"+$('#buff_reWriteItemId').text());
		}
		hideForms();
		clearForms();
});

$("#btnModalItemEditCansel").click(function(){
		$('#modalItemEditClose').modal('hide');
});

$("#btnModalItemDeleteSelected").click(function(){
		itemDeleteSelected();
		$('#modalItemDeleteSelected').modal('hide');
});

$("#buttonLogOut").click(function(){
		queryLogOut();
});


myEditor=CKEDITOR.replace( 'item_content' );
//CKEDITOR.config.extraPlugins = 'mysave';




myEditor.on( 'change', function( evt ) {
    //Указываем действие при изменении контента
	myEditor.ui.get('Save').setState( CKEDITOR.TRISTATE_OFF );
	//alert("ON");
});

myEditor.addCommand("saveItemContent",{
	exec: function(){
		//alert("saveItemContent");
		//alert("SKIN="+CKEDITOR.skin.path());
		if(myEditor.ui.get('Save').getState()==2 ){
			queryItemSave();
		}
		
		myEditor.ui.get('Save').setState( CKEDITOR.TRISTATE_DISABLED );
		
	}
	
});

myEditor.addCommand("closeItemContent",{
	exec: function(){
		//alert("saveItemContent");
		itemEditCloseModal();
	}
	
});


myEditor.ui.addButton( 'Save', {
			label: 'Сохранить',
			command: 'saveItemContent',
			toolbar: 'document',
			icon: '/plugins/ckeditor/skins/moono-lisa/images/save.png'
		});
		
myEditor.ui.addButton( 'Close', {
			label: 'Закрыть',
			command: 'closeItemContent',
			toolbar: 'others',
			icon: '/plugins/ckeditor/skins/moono-lisa/images/close.png'
		});










/*
    //$('.calendarFrom').datetimepicker({
	$('.calendarFrom').datepicker({
		language: 'ru',
		format: 'yyyy-mm-dd',
		autoclose: true,
		todayBtn:false,
		todayHighlight: true
		//keyboardNavigation: false
	}).on('show',function(e){
		//alert("SHOW");	
	}).on('changeDate', function(e) {
        // `e` here contains the extra attributes
    });
	
	$('.calendarTo').datepicker({
		language: 'ru',
		format: 'yyyy-mm-dd',
		autoclose: true,
		todayBtn:false,
		todayHighlight: true
		//keyboardNavigation: false
	}).on('show',function(e){
		//alert("SHOW");	
	}).on('changeDate', function(e) {
        // `e` here contains the extra attributes
    });

*/
	

	//контейнер загрузки файлов contentArea
	$("#drag-and-drop-zone").dmUploader({
	//$("#contentArea").dmUploader({
		url: '/ctrl/ctrl_lk/lk.handler.php',
        dataType: 'json',
        allowedTypes: 'image/*',
		maxFileSize: 2000000,
		extraData: {
			action:'queryUploadImages',
			//itemId:$('#buff_reWriteItemId').text(),
		},
		buffItemId:'#buff_reWriteItemId',
		fileName:'fileUpload',
		onInit: function(){
			//alert("UPLOADER INIT");
			//alert(tinyMCE.activeEditor.id);
		},
		onFileTypeError: function(file){
			//alert("UPLOADER TYPE-ERROR");
			$("#error_preview_errors").html("<div class=\"alert alert-danger\">Вы можете прикреплять только изображения</div>");
		},
		onNewFile: function(id, file){
			clearError("error_preview_errors");
		},
		onEmptyItemError: function(){
			//alert($("#buff_reWriteItemId").text());
			$("#error_preview_errors").html("<div class=\"alert alert-danger\">Для загрузки изображений сохраните страницу</div>");
		},
		
		onFileSizeError: function(file){
			$("#error_preview_errors").html("<div class=\"alert alert-danger\">Вы можете прикреплять файлы размером не более 1,5M</div>");
		},
		
		onBeforeUpload: function(id){
			//alert("UPLOADER BEFOR");
			$("#loader-preview").show();
		},
		onLog:function(myData){
			//alert(myData.get("action"));
		},
		onUploadError: function(id, message){
			//alert("onUploadError= "+message);
		},
		onUploadSuccess: function(id, respond){
			//alert(respond.state);
			//alert(respond.data);
			if(respond.state==-1){
				$("#error_preview_errors").html(respond.data);
			}else{
				$("#preview_files").html(respond.data);
			}
			//надо писать свою функцию вывода изображений
			//parse(data.HTML);
			//parse(); из api
			$("#loader-preview").hide();
		}
		
	});
	
	

});

function editItemDateShow(itemId){
	$("#divTextDate-"+itemId).hide();
	$("#divInputDate-"+itemId).show();
	
	cFrom=$("#itemDateFrom-"+itemId).text();
	cTo=$("#itemDateTo-"+itemId).text();

	//alert(getStringDate(cFrom,2));
	$("#calendarFrom-"+itemId).val(getStringDate(cFrom,2));
	$("#calendarTo-"+itemId).val(getStringDate(cTo,2));
}

function editItemDateHide(itemId){
	
	$("#divInputDate-"+itemId).hide();
	
	cFrom=$("#calendarFrom-"+itemId).val();
	cTo=$("#calendarTo-"+itemId).val();
	
	if(cFrom!=''){
		//$("#itemDateFrom-"+itemId).text(cFrom);
		$("#itemDateFrom-"+itemId).text(getStringDate(cFrom,1));
	}else {
		cFrom='0000-00-00';
		$("#itemDateFrom-"+itemId).text(getStringDate(cFrom,1));
	}
	if(cTo!=''){
		$("#itemDateTo-"+itemId).text(getStringDate(cTo,1));
	}else{
		cTo='0000-00-00';
		$("#itemDateTo-"+itemId).text(getStringDate(cTo,1));
	}
	
	
	$("#calendarFrom-"+itemId).val('');
	$("#calendarTo-"+itemId).val('');
	
	$("#divTextDate-"+itemId).show();
}

function getStringDate(mDate,ver){

	//mDString=mDate.replace(/(\d+)-(\d+)-(\d+)/, '$3.$2.$1');
	
	switch (ver){
		case 1:mDString=mDate.replace(/(\d+)-(\d+)-(\d+)/, '$3.$2.$1');
			break;
		case 2:mDString=mDate.replace(/(\d+).(\d+).(\d+)/, '$3-$2-$1');
			break;
	}
	return 	mDString;
}

function clearError(id){
	$("#"+id).html("");
}

function clearModal(){
	$('#modalItemDeleteId').html("");
	$('#modalItemDeleteName').html("");
}

function clearForms(){
	//очищаем формы перед открытием
	clearError("error_preview_errors");
	$("#item_name").val('');
	$("#item_translit").val('');
	$("#preview_files").html('');
	//CKEDITOR.replace item_content
	//CKEDITOR.instances.item_content.insertHtml('');
	myEditor.setData('');
	myEditor.ui.get('Save').setState( CKEDITOR.TRISTATE_OFF );
}
	
function hideForms(){
	//устанавливаем значения по умолчанию в разделе служебной информации
	$("#buff_reWriteItemId").html('empty');
	$("#formItemLegendCreate").hide();
	$("#formItemLegendEdit").hide();
	$("#formItemPartName").hide();
	$("#formItemPartContent").hide();
	$("#formItemPartImages").hide();
	$("#panelItemCreateOrEdit").hide();
}

function scrolltoForms(){//OK
	$('html, body').animate({scrollTop: $('#anchorForms').offset().top}, 500);
}

function scrolltoAnchor(anchor){
//alert(anchor);
	if($("div").is('#'+anchor)){
		$('html, body').animate({scrollTop: $('#'+anchor).offset().top}, 500);
	}
	
}

function scrolltoTrItem(itemId){
	$('html, body').animate({scrollTop: $('#trItem_'+itemId).offset().top}, 500);
}

function closeForms(){
	clearForms();
	hideForms();
	//scrolltoTableItems();
}

function translate(field1,field2){//OK
	//заполнение descriprion автоматически
	var str=$('#'+field1).val();
	
	str=str.toLowerCase();
	//alert(str);
	var letter_ru =new Array("а","б","в","г","д","е","ё","ж","з","и","й","к", "л","м","н","о","п","р","с","т","у","ф","х","ц","ч",  "ш", "щ", "ъ", "ы","ь","э","ю","я"  );
	var letter_en =new Array("a","b","v","g","d","e","e","zh","z","i","i","k","l","m","n","o","p","r","s","t","u","f","h","zc","ch","sh","sch","" ,"y","", "e","yu","ya");
	//alert(letter_ru.length);
	str=str.replace(/ь/g,"");
	str=str.replace(/Ъ/g,"");
	str=str.replace(/\s/g,"_");
	var m=str;
	for(i=0;i<letter_ru.length;i++){
		var m=m.replace(new RegExp(letter_ru[i],'g'),letter_en[i]);
	}
	$('#'+field2).val(m);
//alert(m);
}

function insertTrItemRow(html){
	//alert("insert");
	$('#tableItems tbody').prepend(html);
	
}

function deleteTrItemRow(itemId){
	//alert("insert");
	$('#trItem_'+itemId).remove();
	
}

function queryItemSave(){//OK
	

var jsonStringOut
var fieldArray=new Object();
var cloneObject=new Object();
var data=new Object();
var dump=new Object();
var object="refresh";
//alert("FUNC");
	
	fieldArray=["item_name","item_content"];
	for(i=0;i<fieldArray.length;i++){
		if($('#'+fieldArray[i]).attr('type')===undefined){
			dump['type']="empty";
		}else{
			dump['type']=$('#'+fieldArray[i]).attr('type');
		}
		
		if(fieldArray[i]=="item_content"){//изменяем непрерывный пробел на <br>, а то не пишет в базу данных
			//var value=$('#'+fieldArray[i]).val();
			var value=myEditor.getData();
			//еще меняем символы, так как encodeURIComponent кодирует не все
			value=value.replace(/\'/g, "&#039;");
			//value=value.replace(/\-/g, "&#150;");
			//value=value.replace(/\_/g, "&#095;");//исключаем так как при парсенге заменяется _ в mod_name
			value=value.replace(/\./g, "&#046;");
			value=value.replace(/\!/g, "&#033;");
			value=value.replace(/\~/g, "&#126;");
			value=value.replace(/\*/g, "&#042;");
			value=value.replace(/\(/g, "&#040;");
			value=value.replace(/\)/g, "&#041;");

			value=encodeURIComponent(value);
			value=value.replace(/\-/g, "(blankSign)");//если заменить сущностью, на работает стиль с тире пример: style="border-color"
			value=value.replace(/%/g, "(percentSign)");
			//alert("TEST"+value);
		}else{
			value=$('#'+fieldArray[i]).val();
			value=mnemonicSymbolsEncode(value);
		}
		
		dump['name']=$('#'+fieldArray[i]).attr('name');
		dump['value']=value;
		//dump['column']=fieldArray[i];
		cloneObject[i]=Clone(dump);
		data[fieldArray[i]]=cloneObject[i];
	}
	data['reWriteItemId']=$('#buff_reWriteItemId').text();
	data['action']="queryItemSave";
	
	jsonStringOut=$.toJSON(data);
	
	
	//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		//datatype:'json',
		beforeSend: function(){$("#loading-"+object).show();},
		
		success: function(jsonStringIn){
		//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);
		
			if(arrayData['state']==1 && arrayData['event']=='new'){
				//insertTrItemRow(arrayData['item']);//если вставлять строки не меняется число страницу
				queryGetPages(1);
				
				//closeForms();
				//scrolltoTrItem(arrayData['itemId']);
				//надо забрать значение itemId чтобы вписать в служебную информацию
				$("#buff_reWriteItemId").text(arrayData['itemId']);
				$("#formItemLegendCreate").hide();
				$("#formItemPartName").hide();
				$("#formItemLegendEdit").show();
			}else if(arrayData['state']==1 && arrayData['event']=='edit'){
				//Надо показать пользователю что данные сохранены
				$("#messageSave").show();
				//closeForms();
				//scrolltoTrItem(arrayData['itemId']);
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}

			//надо добавить закрытие редактора в полноэкранном режиме
			//tinymce.activeEditor.destroy();
			//tinyMCE.activeEditor.close();
		},
		//complete: function(){$("#loading-"+object).hide();  hideForms(); }
		complete: function(){$("#loading-"+object).hide();}
	});
return false;	

}

function editItemNameShow(itemId){
	
	itemName=$("#textItemName_"+itemId).text();
	//alert(itemName);
	$("#divTextItemName_"+itemId).hide();
	$("#divInputItemName_"+itemId).show();
	$("#inputItemName_"+itemId).val(itemName);
	$("#inputItemName_"+itemId).focus();
}

function editItemNameHide(itemId,itemText){
	//alert("Save");
	//itemName=$("#inputItemName_"+itemId).val();
	$("#divInputItemName_"+itemId).hide();
	//$("#textItemName_"+itemId).text(decodeURIComponent(itemText));
	$("#textItemName_"+itemId).html(itemText);
	$("#divTextItemName_"+itemId).show();
	
}

function mnemonicSymbolsEncode(value){
//alert("MNEMONOK");
	
	//value=value.replace(/\./g, "&#046;");
	//value=value.replace(/\!/g, "&#033;");
	//value=value.replace(/\~/g, "&#126;");
	//value=value.replace(/\*/g, "&#042;");
	//value=value.replace(/\(/g, "&#040;");
	//value=value.replace(/\)/g, "&#041;");
	
	//value==value.replace(/\&/g, "&amp;");
	//value=value.replace(/\'/g, "&#039;");
	
	
	
	value=value.replace(/\'/g, "\"");
	//value=encodeURIComponent(value);
return value;
}
function mnemonicSymbolsDecode(value){
//alert("MNEMONOK");

	//value=decodeURIComponent(value);

	//value==value.replace(/\&amp;/g, "&");
	//value=value.replace(/\&#046;/g, ".");
	//value=value.replace(/\&#033;/g, "!");
	//value=value.replace(/\&#126;/g, "~");
	//value=value.replace(/\&#042;/g, "*");
	//value=value.replace(/\&#040;/g, "(");
	//value=value.replace(/\&#041;/g, ")");
	//value=value.replace(/\&#039;/g, "'");
	
	//value=encodeURIComponent(value);
return value;
}



function queryItemNameSave(itemId){
var jsonStringOut;
var fieldArray=new Object();
var cloneObject=new Object();
var data=new Object();
var dump=new Object();
var object="refresh";
//alert("FUNC");
	
	fieldArray=["inputItemName_"+itemId];
	for(i=0;i<fieldArray.length;i++){
		if($('#'+fieldArray[i]).attr('type')===undefined){
			dump['type']="empty";
		}else{
			dump['type']=$('#'+fieldArray[i]).attr('type');
		}
		
		value=$('#'+fieldArray[i]).val();
		value=value.replace(/\+/g, "(plusSign)");
		value=mnemonicSymbolsEncode(value);
		
		//value=encodeURIComponent(value);
		
		dump['name']=$('#'+fieldArray[i]).attr('name');
		dump['value']=value;
		
		//dump['column']=fieldArray[i];
		cloneObject[i]=Clone(dump);
		data[fieldArray[i]]=cloneObject[i];
	}
	data['itemId']=itemId;
	data['action']="queryItemNameSave";
	
	//jsonStringOut=JSON.stringify(data);
	jsonStringOut=$.toJSON(data);
	
	
	//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		//datatype:'json',
		beforeSend: function(){$("#loading-"+object).show();},
		
		success: function(jsonStringIn){
		//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);
		//alert(arrayData['itemTest']);
			if(arrayData['state']==1){
				editItemNameHide(arrayData['itemId'],arrayData['itemText']);
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}

			//надо добавить закрытие редактора в полноэкранном режиме
			//tinymce.activeEditor.destroy();
			//tinyMCE.activeEditor.close();
		},
		complete: function(){$("#loading-"+object).hide();  hideForms(); }
	});
return false;
	
}

function editItemContentShow(itemId){
	clearForms();
	hideForms();
	scrolltoForms();
	$("#formItemLegendEdit").show();
	$("#formItemPartContent").show();
	$("#panelItemCreateOrEdit").show();
	//$("#formItemPartImages").show();
	$("#buff_reWriteItemId").text(itemId);
	
var jsonStringOut;
var data=new Object();
var object;

	//data['pageid']=pageid;
	data['itemId']=itemId;
	data['action']="queryItemEdit";
	
	jsonStringOut=$.toJSON(data);
	//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		
		//beforeSend: function(){$("#loading_"+object).show();},
		
		success: function(jsonStringIn){
			//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);

			//заполнение полей для редактирования полученными данными

			if(arrayData['state']==1){
				//tinyMCE.activeEditor.setContent(arrayData["data"]["item_content"]);
				myEditor.setData(arrayData["data"]["item_content"]);
				$("#preview_files").html(arrayData["data"]["item_imgs"]);
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}
		
		},
		complete: function(){$("#loading_"+object).hide(); }
	});
return false;	
	
}

function itemPubChenge(itemId,flag){
var jsonStringOut;
var data=new Object();
var object;

	//data['pageid']=pageid;
	data['itemId']=itemId;
	data['itemPub']=flag;
	data['action']="queryItemPubChenge";
	
	jsonStringOut=$.toJSON(data);
	//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		
		//beforeSend: function(){$("#loading_"+object).show();},
		
		success: function(jsonStringIn){
			//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);

			//заполнение полей для редактирования полученными данными

			if(arrayData['state']==1){
				//alert("PUB-CHENGE");
				if(arrayData['itemPub']==1){
					$("#pub_"+arrayData['itemId']).removeClass('glyphicon-star-empty');
					$("#pub_"+arrayData['itemId']).addClass('glyphicon-star');
				}
				if(arrayData['itemPub']==0){
					$("#pub_"+arrayData['itemId']).removeClass('glyphicon-star');
					$("#pub_"+arrayData['itemId']).addClass('glyphicon-star-empty');
				}
				
				//arrayData['itemId']
				
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}
		
		},
		complete: function(){$("#loading_"+object).hide(); }
	});
return false;	
	
	
}

function itemDelete(itemId){
	var jsonStringOut;
var data=new Object();
var object;

	//data['pageid']=pageid;
	data['itemId']=itemId;
	data['action']="queryItemDelete";
	
	jsonStringOut=$.toJSON(data);
	//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		
		//beforeSend: function(){$("#loading_"+object).show();},
		
		success: function(jsonStringIn){
			//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);

			//заполнение полей для редактирования полученными данными

			if(arrayData['state']==1){
				//alert("ITEM-DELETE");
				deleteTrItemRow(arrayData['itemId']);				
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}
		
		},
		complete: function(){$("#loading_"+object).hide(); }
	});
return false;	
}

function itemDeleteSelected(){
	var jsonStringOut;
var data=new Object();
var dumpCheckbox=new Object();
var cloneObject=new Object();
var object;
var j=0;

	$(".itemCheckbox").each(function() { 
		if(this.checked == true){
			dumpCheckbox[j]=this.value;
			cloneObject[j]=Clone(dumpCheckbox);
			data['checkbox']=cloneObject[j];
			j=j+1;
		} 
	});
	data['action']="queryItemDeleteSelected";
	
	jsonStringOut=$.toJSON(data);
	//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		
		//beforeSend: function(){$("#loading_"+object).show();},
		
		success: function(jsonStringIn){
			//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);

			//заполнение полей для редактирования полученными данными

			if(arrayData['state']==1){
				//alert("ITEM-DELETE");
				
				for(i=0; i<arrayData['itemsId'].length;i++){
					deleteTrItemRow(arrayData['itemsId'][i]);
				}
				
				
								
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}
		
		},
		complete: function(){$("#loading_"+object).hide(); }
	});
return false;	
	
	
	
}

function itemPubChengeSelected(flag){
var jsonStringOut;
var data=new Object();
var dumpCheckbox=new Object();
var cloneObject=new Object();
var object;
var j=0;

	$(".itemCheckbox").each(function() { 
		if(this.checked == true){
			dumpCheckbox[j]=this.value;
			cloneObject[j]=Clone(dumpCheckbox);
			data['checkbox']=cloneObject[j];
			j=j+1;
		} 
	});
	data['itemPub']=flag;
	data['action']="queryItemPubChengeSelected";
	
	jsonStringOut=$.toJSON(data);
	//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		
		//beforeSend: function(){$("#loading_"+object).show();},
		
		success: function(jsonStringIn){
			//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);

			//заполнение полей для редактирования полученными данными

			if(arrayData['state']==1){
				//alert("ITEM-PUBCHENGES");
				
				for(i=0; i<arrayData['itemsId'].length;i++){
					if(arrayData['itemsPub']==1){
						$("#pub_"+arrayData['itemsId'][i]).removeClass('glyphicon-star-empty');
						$("#pub_"+arrayData['itemsId'][i]).addClass('glyphicon-star');
					}
					if(arrayData['itemsPub']==0){
						$("#pub_"+arrayData['itemsId'][i]).removeClass('glyphicon-star');
						$("#pub_"+arrayData['itemsId'][i]).addClass('glyphicon-star-empty');
					}
				}
				
				$(".itemCheckbox").each(function() { 
					if(this.checked == true){
						this.checked=false;
					} 
				});
				
								
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}
		
		},
		complete: function(){$("#loading_"+object).hide(); }
	});
return false;	
	
	
}

function buttonChekedItem(check){
	if(check==1){
		$(".itemCheckbox").each(function() { 
		this.checked = true;
		});
		
	}else{
		$(".itemCheckbox").each(function() { 
		this.checked = false;
		});
	}
	
	
}

function imgDelete(itemId,imgId){
	var jsonStringOut;
var data=new Object();
var object;

	//data['pageid']=pageid;
	data['imgId']=imgId;
	data['itemId']=itemId;
	data['action']="queryImgDelete";
	
	jsonStringOut=$.toJSON(data);
	//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		
		//beforeSend: function(){$("#loading_"+object).show();},
		
		success: function(jsonStringIn){
			//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);

			//заполнение полей для редактирования полученными данными

			if(arrayData['state']==1){
				//alert("ITEM-DELETE");
				$("#preview_files").html(arrayData["data"]);				
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}
		
		},
		complete: function(){$("#loading_"+object).hide(); }
	});
return false;	
}

function itemDeleteModal(itemId){
//textItemName_{itemId}
	$('#modalItemDeleteId').html(itemId);
	$('#modalItemDeleteName').html($('#textItemName_'+itemId).html());
	
	$('#modalItemDelete').modal('toggle');
	//itemDelete(itemId);
	//$('#modalItemDelete').modal('hide');
}

function queryItemTimePubSave(itemId){
var jsonStringOut;
var data=new Object();
var object;
	
	data['itemId']=itemId;
	data['dateFrom']=$('#calendarFrom-'+itemId).val();
	data['dateTo']=$('#calendarTo-'+itemId).val();
	data['action']="queryItemTimePubSave";
	
	jsonStringOut=$.toJSON(data);
	
	
	//alert(jsonStringOut);

	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		//datatype:'json',
		beforeSend: function(){$("#loading-"+object).show();},
		
		success: function(jsonStringIn){
		//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);
		
			if(arrayData['state']==1){
				//editItemNameHide(arrayData['itemId']);
				//alert("DATE CHANGE");
				editItemDateHide(arrayData['data'])
			}else{
				//parse(jsonStringIn); //api
				//alert("beginParse");
			}
		},
		complete: function(){$("#loading-"+object).hide();  hideForms(); }
	});
return false;

}

function queryGetView(page){
var cloneObject=new Object();
var data=new Object();
var dump=new Object();

	data['page']=page;
	data['action']="queryGetView";

	jsonStringOut=$.toJSON(data);
	
//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		//datatype:'json',
		beforeSend: function(){
			//$("#loading-"+object).show();
		},
		
		success: function(jsonStringIn){
		//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);
		
			if(arrayData['state']==1){
				$("#itemsTable").html(arrayData['data']);
				//scrolltoAnchor('anchorTop');
				//queryGetPages();
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}

		},
		complete: function(){
			//$("#loading-"+object).hide();  hideForms(); 
		}
	});
return false;
}

function queryGetPages(page){
var cloneObject=new Object();
var data=new Object();
var dump=new Object();

	data['PAGE']=page;
	data['action']="queryGetPages";

	jsonStringOut=$.toJSON(data);
	
//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		//datatype:'json',
		beforeSend: function(){
			//$("#loader-itemsTable").show();
			progressLoader("itemsTable",1);
		},
		
		success: function(jsonStringIn){
		//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);
		
			if(arrayData['state']==1){
				$("#itemsPages").html(arrayData['data']);
				queryGetView(page);
				//scrolltoAnchor('anchorTop');
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}

		},
		complete: function(){
			//$("#loader-itemsTable").hide();
			progressLoader("itemsTable",0);			
		}
	});
return false;

}

function itemEditCloseModal(){
	$('#modalItemEditClose').modal('toggle');
}

function itemDeleteSelectedModal(){
	$('#modalItemDeleteSelected').modal('toggle');
}

function queryLogOut(){
var data=new Object();
	data['action']="queryLogOut";

	jsonStringOut=$.toJSON(data);
//alert(jsonStringOut);
	$.ajax({   
		type: "POST",
		url: "/ctrl/ctrl_lk/lk.handler.php",   
		data: 'jsonStringIn='+jsonStringOut,
		//datatype:'json',
		beforeSend: function(){
			//$("#loading-"+object).show();
		},
		
		success: function(jsonStringIn){
		//alert(jsonStringIn);
			arrayData=$.evalJSON(jsonStringIn);
		
			if(arrayData['state']==1){
				window.location.replace(arrayData['data']);
			}else{
				parse(jsonStringIn); //api
				//alert("beginParse");
			}

		},
		complete: function(){
			//$("#loading-"+object).hide();  hideForms(); 
		}
	});
return false;
}

function progressLoader(obj,state){
	switch(state){
		case 1: 
			$("#loader-"+obj).show();
			break;
		case 0:
			$("#loader-"+obj).hide();
			break;
		default:
			$("#loader-"+obj).hide();
		
	}
}




